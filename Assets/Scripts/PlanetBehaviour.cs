﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetBehaviour : MonoBehaviour {

    public float period;
    public float radius;
    private float angle;
	void Start () {
        transform.position = new Vector3(radius, 0f, 0f);
        angle = 0;
	}
	
	void Update () {
		
	}

    private void FixedUpdate()
    {
        angle += 360*(Time.deltaTime/period);
        float x = radius * Mathf.Cos(angle * 0.0174533f);
        float y = radius * Mathf.Sin(angle * 0.0174533f);
        transform.position = new Vector3(x, y, 0f);
    }
}
