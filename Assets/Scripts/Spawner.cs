﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public PlanetPhysicBehaviour moon;
    public PlanetPhysicBehaviour earse;
    public float G;

    private void Awake()
    {
        //createO();        

    }
    void createO()
    {
        float f = 1f;
        float d = 2f;
        PlanetPhysicBehaviour m1 = GameObject.Instantiate(moon);
        m1.transform.parent = transform;
        m1.transform.position = new Vector3(d, 0f, 0f);
        m1.initialForce = new Vector3(0f, -f, 0f);

        PlanetPhysicBehaviour m2 = GameObject.Instantiate(moon);
        m2.transform.parent = transform;
        m2.transform.position = new Vector3(-d, 0, 0f);
        m2.initialForce = new Vector3(0f, f, 0f);

        PlanetPhysicBehaviour m3 = GameObject.Instantiate(moon);
        m3.transform.parent = transform;
        m3.transform.position = new Vector3(0f, d, 0f);
        m3.initialForce = new Vector3(f, 0f, 0f);

        PlanetPhysicBehaviour m4 = GameObject.Instantiate(moon);
        m4.transform.parent = transform;
        m4.transform.position = new Vector3(0f, -d, 0f);
        m4.initialForce = new Vector3(-f, 0f, 0f);

    }
    void createInGrid()
    {
        for (int i = -3; i <= 3; i++)
        {
            for (int j = -3; j <= 3; j++)
            {
                PlanetPhysicBehaviour m1 = GameObject.Instantiate(moon);
                m1.transform.parent = transform;
                m1.transform.position = new Vector3((float)j, (float)i, 0f);
                m1.initialForce = new Vector3(0f, 0f, 0f);
            }
            
        }
    }

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
