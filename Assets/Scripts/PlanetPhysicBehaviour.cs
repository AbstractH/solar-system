﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetPhysicBehaviour : MonoBehaviour {
    public Vector3 initialForce;
    private List<PlanetPhysicBehaviour> planets;
    private Rigidbody rb;
    private BoxCollider bc;
    private Spawner s;

	void Start () {
        bc = GetComponentInParent<BoxCollider>();
        s = GetComponentInParent<Spawner>();
        rb = GetComponent<Rigidbody>();
        rb.AddForce(initialForce);
        planets = new List<PlanetPhysicBehaviour>();
        for(int i = 0; i < transform.parent.childCount; i++)
        {
            Transform c = transform.parent.GetChild(i);
            if (this.transform != c)
            {
                planets.Add(c.GetComponent<PlanetPhysicBehaviour>());
            }
            else
            {
                Debug.Log(c.name);
            }
        }
	}
	
	void Update () {
		
	}

    private void FixedUpdate()
    {
        loopSpace();
        Vector3 force = new Vector3(0f, 0f, 0f);
        foreach(PlanetPhysicBehaviour planet in planets)
        {
            //F = G*(m1*m2/r^2)
            
            float m1 = planet.GetComponent<Rigidbody>().mass;
            float m2 = rb.mass;
            Vector3 r = planet.transform.position - transform.position;
            force += calculateGravityVector(m1, m2, r.magnitude, r.normalized);
            List<Vector3> conpansiveVectors = calculateConpensiveVectors(r);
            debugVectors(conpansiveVectors);
            foreach(Vector3 v in conpansiveVectors)
            {
                force += calculateGravityVector(m1, m2, v.magnitude, v.normalized);
            }
            
        }
        rb.AddForce(force);
    }
    void loopSpace()
    {
        Vector3 p = transform.position;
        if (Mathf.Abs(p.x) > bc.size.x / 2)
            transform.position = p = new Vector3(-p.x, p.y, p.z);
        if (Mathf.Abs(p.y) > bc.size.y / 2)
            transform.position = new Vector3(p.x, -p.y, p.z);
    }
    private void debugVectors(List<Vector3> vectors)
    {
        if (transform.name == "Moon")
        {
            foreach(Vector3 v in vectors)
            {
                Debug.DrawLine(transform.position, transform.position + v, Color.red, 0.01f);
            }
        }
    }
    private Vector3 calculateGravityVector(float m1, float m2, float distance, Vector3 direction)
    {
        float G = s.G;
        return direction.normalized * (G * (m1 * m2) / (distance*distance)) * Time.deltaTime;
    }

    private List<Vector3> calculateConpensiveVectors(Vector3 r)
    {
        List<Vector3> result = new List<Vector3>();
        Vector3 bcx = new Vector3(Mathf.Sign(-r.x) * bc.size.x,0f,0f);
        Vector3 bcy = new Vector3(0f, Mathf.Sign(-r.y) * bc.size.x,0f);
        Vector3 rx = new Vector3(r.x, 0f, 0f);
        Vector3 ry = new Vector3(0f, r.y, 0f);
        Vector3 f1 = rx + bcy + ry;
        Vector3 f2 = bcx + rx + bcy + ry;
        Vector3 f3 = bcx + rx + ry;
        result.Add(f1);
        result.Add(f2);
        result.Add(f3);
        return result;
    }

    private void OnTriggerExit(Collider other)
    {
        /*if (other.name == "System") {
            Vector3 p = transform.position;
            if (Mathf.Abs(p.x) > bc.size.x/2)
                transform.position = p = new Vector3(-p.x, p.y, p.z);
            if (Mathf.Abs(p.y) > bc.size.y/2)
                transform.position = new Vector3(p.x, -p.y, p.z);
        }*/
    }
    private void OnCollisionStay(Collision collision)
    {
        //if (collision.collider.name == "System") return;
        PlanetPhysicBehaviour planet = collision.collider.transform.GetComponent<PlanetPhysicBehaviour>();
        float m1 = planet.GetComponent<Rigidbody>().mass;
        float m2 = rb.mass;
        Vector3 r = transform.position - planet.transform.position;
        float k = (planet.name == "Sun") ? 100f : 10f;
        rb.AddForce(r.normalized * 3f);

    }
}
